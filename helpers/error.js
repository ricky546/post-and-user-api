function error(res, statusCode, error) {
    return res.status(statusCode).json({
        status: false,
        errors: error
    })
}
module.exports = error