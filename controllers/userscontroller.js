const User = require('../models/user.js')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const succes = require('../helpers/succes')
const error = require('../helpers/error.js')

let create = (req, res) => {
    let {email, username, password} = req.body
    let encryptedPassword = bcrypt.hashSync(password, 10)
    let user = new User({
        email, username, encryptedPassword
    })
    
    user.save()
    .then(() => {
        succes(res, 201, user)
    })
    .catch(err => {
        error(res, 422, err)
    })
}

let find = (req, res) => {
    User.find()
    .then(data => {
        succes(res, 200, data)
    })
    .catch(err => {
        error(res, 404, err)
    })
}

let findById= (req, res) => {
    User.find({_id:req.params._id})
    .then(data => {
        succes(res, 200, data)
    })
    .catch(err => {
        error(res, 404, err)
    })
}

let update = (req, res) => {
    User.findOneAndUpdate({_id:req.params._id}, req.body)
    .then(data => {
        succes(res, 200, data)
    })
    .catch(err => {
        error(res, 404, err)
    })
}

let remove = (req, res) => {
    User.findOneAndDelete({_id: req.params._id})
    .then(data => {
        error(res, 200, data)
    })
    .catch(err => {
        error(res, 404, err)
    })
}

let login = (req, res) => {
    User.findOne({email:req.body.email})
    .then(data => {
        let {password} = req.body
        let loginPass = bcrypt.compareSync(password, data.encryptedPassword)
        if (loginPass === true) {
            let payload = jwt.sign({_id: data._id, email: data.email}, process.env.SECRET_KEY) 
            res.status(200).json({
                status: true,
                data: {
                    _id: data._id,
                    email: data.email,
                    token: payload
                }
            })
        } else {
            error(res, 400, 'Wrong Password!')
        }
        
    })
    .catch(err => {
        error(res, 422, err)
    })
}


module.exports = {
    create,
    find,
    findById,
    update,
    remove,
    login
}