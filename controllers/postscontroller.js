const Post = require('../models/post.js')

let create = (req, res) => {
    let {title, body} = req.body;
    let author = req.headers.authorization;
    let post = new Post({
        author ,title, body
    })
    post.save()
    .then(() => {
        res.status(201).json({
            status: true,
            data: post
        })
    })
    .catch(err => {
        res.status(422).json({
            status: false,
            errors: err
        })
    })
}

let read = (req, res) => {
    Post.find()
    .populate({
        path: 'author',
        select: '-password'
    })
    .populate({
        path: 'likes',
        select: '-password'
    })
    .then(data => {
        res.status(200).json({
            status: true,
            data
        })
    })
    .catch(err => {
        res.status(404).json({
            status: false,
            errors: err
        })
    })
}

let findById = (req, res) => {
    Post.find({_id: req.params._id})
    .then(data => {
        res.status(200).json({
            status: true,
            data
        })
    })
    .catch(err => {
        res.status(404).json({
            status: false,
            errors: err
        })
    })
}

let like = (req, res) => {
    Post.findOne({_id: req.params._id})
    .then(data => {
        if (data.likes.indexOf(req.headers.authorization) > -1) {
            return res.status(400).json({
                status: false,
                errors: 'you\'ve already liked this post.'
            })
        }
        data.likes.push(req.headers.authorization)
        data.save()
        .then((data) => {
            res.status(200).json({
                status: true,
                data: data
            })
        })
        .catch(err => {
            res.status(422).json({
                status: false,
                errors: err
            })
        })
    })
    .catch(err => {
        res.status(400).json({
            status: false,
            errors: err
        })
    })
}

let update = (req, res) => {
    Post.findOneAndUpdate({_id: req.params._id}, req.body)
    .then(data => {
        res.status(200).json({
            status: true,
            data
        })
    })
    .catch(err => {
        res.status(404).json({
            status: false,
            error: err
        })
    })
}

let remove = (req, res) => {
    Post.findOneAndDelete({_id: req.params._id})
    .then(data => {
        res.status(200).json({
            status: true,
            data: data
        })
    })
    .catch(err => {
        res.status(422).json({
            status: false,
            error: err
        })
    })
}



module.exports = {
    create,
    findById,
    update,
    remove,
    read,
    like
}