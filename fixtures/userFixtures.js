const faker = require('faker')
const bcrypt = require('bcrypt')
var password = faker.internet.password()
let create = {
    email: faker.internet.email(),
    username: faker.name.findName(),
    encryptedPassword: bcrypt.hashSync(password, 10)
}

module.exports = create