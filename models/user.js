const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email:{
        type: 'string',
        required: true
    },
    username:{
        type: 'string',
        required: true
    },
    encryptedPassword:{
        type: 'string',
        required: true
    }
    
})

const User = mongoose.model('User', userSchema);

module.exports = User