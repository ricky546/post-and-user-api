const express = require('express');
const router = express.Router();
const authenticate = require('./middlewares/authenticate.js')

const post = require('./controllers/postscontroller.js')
router.post('/posts',authenticate, post.create)
router.get('/posts/:_id', post.findById)
router.put('/posts/:_id', post.update)
router.delete('/posts/:_id', post.remove)
router.get('/posts', post.read)
router.post('/posts/:_id', post.like)

const user = require('./controllers/userscontroller.js')
router.post('/users', user.create)
router.get('/users', user.find)
router.get('/users/:_id', user.findById)
router.put('/users/:_id', user.update)
router.delete('/users/:_id', user.remove)
router.post('/users/login', user.login)

module.exports = router;