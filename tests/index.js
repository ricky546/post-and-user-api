const chai = require('chai')
const {
    expect,
    should
} = chai;
chai.use(require('chai-http'))
const app = ('../index.js')

describe('Root Endpoint', () => {
    it('Should return true and give hello world message', () => {
        chai.request(app)
        .get('/')
        .end((err, res) => {
            console.log(res.body)
            expect(res.status).to.equal(200)
            expect(res.body).to.be.an('object')
            expect(res.body.status).to.be.equal(true)
            expect(res.body.data).to.be.equal('hello World')
        })
    }) 
})