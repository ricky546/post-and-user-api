const chai = require('chai')
const {
    expect,
    should
} = chai;
chai.use(require('chai-http'))
const app = require('../index.js')
let User = require('../models/user.js')
const bcrypt = require('bcrypt')
const faker = require('../fixtures/userFixtures.js')

describe('User Collection', function() {

    beforeEach(function() {
        return User.deleteMany({})
    })

    afterEach(function() {
        return User.deleteMany({})
    })
    context('POST /api/v1/users', function() {
        it('Should create new user', function() {
            var user = new User ({
                email:"ricky123456@gmail.com",
                username:"ricky123456",
                password:"123456"
            })
            chai.request(app)
            .post('/api/v1/users')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                console.log(res.body)
                expect(res.status).to.equal(201)
                expect(res.body).to.be.an('object')
                expect(res.body.status).to.equal(true)
            })
        })
        it('Should not create new user', function() {
            var user = new User ({
                email:"ricky123456@gmail.com",
                username:"ricky123456",
                password:"123456"
            })
            
            chai.request(app)
            .post('/api/v1/users')
            .set('Content-Type', 'application/json')
            .send(JSON.stringify(user))
            .end((err, res) => {
                expect(res.status).to.equal(422)
                expect(res.body).to.not.be.an('object')
                expect(res.body.status).to.equal(false)
                expect(res.body.data.email).to.not.be.an('string')
                expect(res.body.data.username).to.not.be.an('string')
            })
        })
    })
    context('POST /api/v1/users/login', function() {
        it('Should login succesfully!', function() {
            
            let login = {
                email: 'ricky123@gmail.com',
                password: '123456'
            }
            var user = new User ({
                email:"ricky123456@gmail.com",
                username:"ricky123456",
                password:"123456"
            })
            user.save()
            .then((data) => {
                chai.request(app)
                .post('/api/v1/users/login')
                .set('Content-Type', 'application/json')
                .send(JSON.stringify(login))
                .end((err, res) => {
                    expect(res.status).to.equal(200)
                    expect(res.body.status).to.equal(true)
                })
            })

        })
    })
})