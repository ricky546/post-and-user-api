const express = require('express')
const app = express()
const swaggerUI = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument))
const mongoose = require('mongoose')
const router = require('./router.js')
const morgan = require('morgan')
const dotenv = require('dotenv')
dotenv.config()
const env = process.env.NODE_ENV || 'development'
const dbConnectionString = {
    development: process.env.DB_CONNECTION,
    test: process.env.DB_CONNECTION_TEST,
    staging: process.env.DB_CONNECTION_STAGING,
    production: process.DB_CONNECTION_PRODUCTION
}
app.use(morgan('dev'))
app.use(express.json())
app.use('/api/v1', router)
app.use(express.static('public'))
mongoose.connect(dbConnectionString[env], {useNewUrlParser: true, useUnifiedTopology: true})
.then(() => console.log('database Connected'))
.catch((err) => console.log(err)) 

app.get('/', function(req, res) {
    res.status(200).json({
        status: true,
        data:'hello World'
    })
})

module.exports = app